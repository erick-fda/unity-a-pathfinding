﻿/*========================================================================================
    Agent                                                                            *//**
	
    An AI agent that uses A* pathfinding and a movement algorithm to navigate to a goal.
	
    Copyright 2017 Erick Fernandez de Arteaga. All rights reserved.
        https://www.linkedin.com/in/erick-fda
        https://bitbucket.org/erick-fda
        
	@author Erick Fernandez de Arteaga
	@file
	
*//*====================================================================================*/

/*========================================================================================
	Dependencies
========================================================================================*/
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*========================================================================================
	Agent
========================================================================================*/
/**
    An AI agent that uses A* pathfinding and a movement algorithm to navigate to a goal.
*/
public class Agent : MonoBehaviour
{
    /*====================================================================================
		Instance Fields
	====================================================================================*/
	public GameObject _pathMarker;
    public bool _canMoveDiagonally = true;

    public float MAX_SPEED = 5;
    public float MAX_ANGULAR_SPEED = 5;
    public float NEXT_TARGET_SWITCH_DISTANCE = 0.1f;
    public float MOVEMENT_START_DELAY = 3;
    
    /*====================================================================================
		Instance Properties
	====================================================================================*/
    public Text _messageBox { get; set; }

    public List<List<MapNode>> _map { get; set; }
    public MapNode _startNode { get; set; }
    public MapNode _goalNode { get; set; }

    private List<MapNode> _openList { get; set; }
    private List<MapNode> _closedList { get; set; }
    private int _g { get; set; }
    private float _root2 { get; set; }
    private bool _pathFound { get; set; }

    private List<MapNode> _optimalPath { get; set; }
    private MapNode _nextTarget { get; set; }
    private bool _goalReached { get; set; }
    
	/*====================================================================================
		Methods
	====================================================================================*/
	/*------------------------------------------------------------------------------------
		MonoBehaviour Methods
	------------------------------------------------------------------------------------*/
    private void Start()
    {
        /* Initialize values. */
        _openList = new List<MapNode>();
        _closedList = new List<MapNode>();
        _g = 0;
        _root2 = Mathf.Sqrt(2);
        _pathFound = false;
        _optimalPath = new List<MapNode>();
        _goalReached = false;

        Pathfinding();

        /* If a path was found, display an appropriate message and markers along the 
            optimal path. */
        if (_pathFound)
        {
            _messageBox.text = "Found a path to the goal!\n\n(Change map file and press spacebar to replay with a different map.)";
            GetOptimalPath();
            _nextTarget = _startNode;
        }
        else
        {
            _messageBox.text = "No path to the goal was found!\n\n(Change map file and press spacebar to replay with a different map.)";
        }
    }

    private void Update()
    {
        /* Don't do anything on update if there is no path to follow or if the goal has 
            been reached. */
        if (!_pathFound || _goalReached)
        {
            return;
        }

        /* Wait for a delay before starting movement. */
        if (MOVEMENT_START_DELAY > 0)
        {
            MOVEMENT_START_DELAY -= Time.deltaTime;
            return;
        }

        /* If we are close enough to the target, switch to the next target along the 
            path. */
        if ((transform.position - _nextTarget._position).magnitude < NEXT_TARGET_SWITCH_DISTANCE)
        {
            _optimalPath.Remove(_nextTarget);
            
            /* If there are targets left on the path, get the next one. If there are no 
                targets left, we have reached the goal. */
            if (_optimalPath.Count > 0)
            {
                _nextTarget = _optimalPath[0];
            }
            else
            {
                _goalReached = true;
            }
        }

        /* Get the movement for this frame. */
        Vector3 movement = (_nextTarget._position - transform.position).normalized * MAX_SPEED * Time.deltaTime;

        /* Apply movement. */
        transform.position += movement;

        /* Turn to face the direction of motion. */
        if (movement != Vector3.zero) 
        {
            transform.rotation = Quaternion.Slerp(
                transform.rotation,
                Quaternion.LookRotation(movement, Vector3.back),
                Time.deltaTime * MAX_ANGULAR_SPEED
            );
        }
    }
    
	/*------------------------------------------------------------------------------------
		Instance Methods
	------------------------------------------------------------------------------------*/
    /**
        Finds the optimal path to the goal using A* pathfinding.
     */
	private void Pathfinding()
    {
        MapNode currentNode = null;
        _openList.Add(_startNode);
        _startNode._g = 0;

        /* Keep checking nodes as long as there are more nodes to check. */
        while (_openList.Count > 0)
        {
            /* The next node we check will always be the open node with the lowest F 
                value. */
            _openList.Sort((first, second) => first._f.CompareTo(second._f));
            currentNode = _openList[0];

            /* Move the current node to the closed list. */
            _closedList.Add(currentNode);
            _openList.Remove(currentNode);

            /* If the current node is the goal, break out of the loop. */
            if (currentNode == _goalNode)
            {
                _pathFound = true;
                break;
            }

            /* Get all nodes that are reachable from the current node. */
            List<MapNode> reachableNodes = GetReachableNodes(currentNode);
            _g++;

            /* Check whether each reachable node should be ignored, added to the open 
                list, or updated. */
            foreach (MapNode eachNode in reachableNodes)
            {
                /* Ignore the node if it's already on the closed list. */
                if (_closedList.Contains(eachNode))
                {
                    continue;
                }

                /* If the node hasn't been looked at yet, add it to the open list, 
                    setting the current node as its parent. */
                if (!_openList.Contains(eachNode))
                {
                    /* Set each node's G value from the current node depending on whether 
                        it is lateral if diagonal to the current node. */
                    if (eachNode.IsDiagonallyAdjacentTo(currentNode))
                    {
                        eachNode._g = currentNode._g + _root2;
                    }
                    else
                    {
                        eachNode._g = currentNode._g + 1;
                    }
                    // eachNode._g = currentNode._g;
                    eachNode.CalculateH(_goalNode._coordinates);
                    eachNode._parent = currentNode;
                    _openList.Add(eachNode);
                }
                /* If the node has been checked before, see if our current G is lower 
                    than its. If so, update its G value and set the current node as its 
                    parent. */
                else
                {
                    float gFromCurrentNode;

                    if (eachNode.IsDiagonallyAdjacentTo(currentNode))
                    {
                        gFromCurrentNode = currentNode._g + _root2;
                    }
                    else
                    {
                        gFromCurrentNode = currentNode._g + 1;
                    }

                    if (gFromCurrentNode < eachNode._g)
                    // if (_g < eachNode._g)
                    {
                        eachNode._g = gFromCurrentNode;
                        // eachNode._g = _g;
                        eachNode._parent = currentNode;
                    }
                }
            }
        }
    }

    /**
        Returns a list of all nodes that are reachable from the given node.
        Reachable in this case means clear (not an obstacle) and immediately adjacent in 
        one of the cardinal directions.
     */
    private List<MapNode> GetReachableNodes(MapNode currentNode)
    {
        /* Get the coordinates adjacent to the given node. */
        List<MapNode> reachableNodes = new List<MapNode>();
        List<IntVector3> adjacentCoordinates = GetAdjacentCoordinates(currentNode._coordinates, _canMoveDiagonally);

        /* Check each adjacent coordinate and keep only those that are within the bounds 
            of the map and clear. */
        foreach (IntVector3 eachCoordinate in adjacentCoordinates)
        {
            int x = eachCoordinate.x;
            int y = eachCoordinate.y;

            /* Ignore if this coordinate falls outside the map. */
            if (x < 0 || 
                x >= _map[0].Count || 
                y < 0 || 
                y >= _map.Count)
            {
                continue;
            }

            MapNode candidateNode = _map[y][x];

            /* Ignore if this coordinate points to a non-open node. */
            if (!candidateNode._isClear)
            {
                continue;
            }

            /* Ignore if the nodes are diagonal to each other and the path between the 
                two nodes is blocked by obstacles. */
            if (currentNode.IsDiagonallyAdjacentTo(candidateNode) && 
                IsPathBlockedByObstacles(currentNode._coordinates, candidateNode._coordinates))
            {
                Debug.Log("Ignoring corner cut!");
                continue;
            }

            reachableNodes.Add(candidateNode);
        }

        return reachableNodes;
    }

    /**
        Returns a list of the coordinates adjacent to the given coordinates.
     */
    private List<IntVector3> GetAdjacentCoordinates(IntVector3 coordinates, bool includeDiagonals)
    {
        List<IntVector3> adjacentCoordinates = new List<IntVector3>();

        /* Get coordinates above, below, left, and right. */
        adjacentCoordinates.Add(new IntVector3(coordinates.x, coordinates.y + 1, coordinates.z));
        adjacentCoordinates.Add(new IntVector3(coordinates.x + 1, coordinates.y, coordinates.z));
        adjacentCoordinates.Add(new IntVector3(coordinates.x, coordinates.y - 1, coordinates.z));
        adjacentCoordinates.Add(new IntVector3(coordinates.x - 1, coordinates.y, coordinates.z));

        /* Get diagonals. */
        if (includeDiagonals)
        {
            adjacentCoordinates.Add(new IntVector3(coordinates.x + 1, coordinates.y + 1, coordinates.z));
            adjacentCoordinates.Add(new IntVector3(coordinates.x + 1, coordinates.y - 1, coordinates.z));
            adjacentCoordinates.Add(new IntVector3(coordinates.x - 1, coordinates.y - 1, coordinates.z));
            adjacentCoordinates.Add(new IntVector3(coordinates.x - 1, coordinates.y + 1, coordinates.z));
        }

        return adjacentCoordinates;
    }

    /**
        Returns whether there are obstacles between two given diagonal coordinates.
     */
    private bool IsPathBlockedByObstacles(IntVector3 a, IntVector3 b)
    {
        /* If either of the cardinally adjacent nodes shared by the given nodes is not 
            clear, then there is an obstacle. */
        if (a.x >= 0 && 
            a.x < _map[0].Count && 
            b.y >= 0 && 
            b.y < _map.Count && 
            !_map[b.y][a.x]._isClear)
        {
            return true;
        }

        if (b.x >= 0 && 
            b.x < _map[0].Count && 
            a.y >= 0 && 
            a.y < _map.Count && 
            !_map[a.y][b.x]._isClear)
        {
            return true;
        }

        return false;
    }

    /**
        Collects and orders the nodes that make up the optimal path.
        Also places a path marker at each node to make the path visible.
     */
    private void GetOptimalPath()
    {
        MapNode nextNode = _goalNode;

        while (nextNode != null)
        {
            Instantiate(_pathMarker, 
                        new Vector3(nextNode._coordinates.x, nextNode._coordinates.y, nextNode._coordinates.z), 
                        Quaternion.identity);

            _optimalPath.Add(nextNode);

            nextNode = nextNode._parent;
        }

        /* We got the path nodes starting at the goal. Reverse the list so that the nodes 
            are ordered starting at the start. */
        _optimalPath.Reverse();
    }
}
