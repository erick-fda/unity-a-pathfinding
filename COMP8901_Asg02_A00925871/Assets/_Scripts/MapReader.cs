﻿/*========================================================================================
    MapReader                                                                        *//**
	
    Reads a text file which contains a map to be used for pathfinding.
	
    Copyright 2017 Erick Fernandez de Arteaga. All rights reserved.
        https://www.linkedin.com/in/erick-fda
        https://bitbucket.org/erick-fda
        
	@author Erick Fernandez de Arteaga
	@file
	
*//*====================================================================================*/

/*========================================================================================
	Dependencies
========================================================================================*/
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*========================================================================================
	Asg02
========================================================================================*/
/**
    Reads a text file which contains a map to be used for pathfinding.
*/
public class MapReader : MonoBehaviour
{
    /*====================================================================================
		Instance Fields
	====================================================================================*/
	public string _backupMapFileName = "map";
    public GameObject _wallPrefab;
    public GameObject _goalPrefab;
    public GameObject _agentPrefab;
    public Text _messageBox;
    
    /*====================================================================================
		Instance Properties
	====================================================================================*/
	private TextAsset _backupMapFile { get; set; }
    private string _mapString { get; set; }

    private List<List<MapNode>> _map { get; set; }
    private MapNode _startNode { get; set; }
    private MapNode _goalNode { get; set; }

    private Agent _agent { get; set; }
    
	/*====================================================================================
		Methods
	====================================================================================*/
	/*------------------------------------------------------------------------------------
		MonoBehaviour Methods
	------------------------------------------------------------------------------------*/
    private void Start()
    {
        /* Initialize fields. */
        _backupMapFile = null;
        _mapString = "";
        _map = new List<List<MapNode>>();

        LoadMapText();
        GenerateMap();
        InitializeAgent();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene(0);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    
	/*------------------------------------------------------------------------------------
		Instance Methods
	------------------------------------------------------------------------------------*/
    /**
        Loads the map from the text file as a string.
     */
	private void LoadMapText()
    {
        /* If the map.txt file exists, load its text. Otherwise, load a default map. */
        if (File.Exists(Application.dataPath + "/Maps/map.txt"))
        {
            _mapString = File.ReadAllText(Application.dataPath + "/Maps/map.txt");
        }
        else
        {
            _backupMapFile = Resources.Load(_backupMapFileName) as TextAsset;
            _mapString = _backupMapFile.text;
        }
    }
    
    /**
        Populates the map and generates appropriate GameObjects using the map string.
     */
    private void GenerateMap()
    {
        /* Split the string into lines. */
        string[] lines = _mapString.Split('\n');
        
        /* Reverse the order of lines so we get the bottom of the map first. */
        Array.Reverse(lines);

        int rowNum = 0;
        int columnNum = 0;

        /* For each char in each line of the map text, create an appropriate map node, 
            add it to the map, and generate an appropriate GameObject for it. */
        foreach (string eachLine in lines)
        {
            /* Start a new row. */
            columnNum = 0;
            _map.Add(new List<MapNode>());

            /* Process each char in the line. */
            foreach (char eachChar in eachLine)
            {
                IntVector3 nodeCoordinates = new IntVector3(columnNum, rowNum, 0);
                bool isNodeClear = false;
                bool isNodeStart = false;
                bool isNodeGoal= false;

                /* Set initial values and instantiate a prefab based on each char. */
                switch (eachChar)
                {
                    case ' ':
                        isNodeClear = true;
                        break;
                    
                    case 'X':
                        Instantiate(_wallPrefab, new Vector3(columnNum, rowNum, 1), Quaternion.identity);
                        break;
                    
                    case 'S':
                        isNodeClear= true;
                        isNodeStart = true;
                        break;
                    
                    case 'G':
                        isNodeClear = true;
                        isNodeGoal = true;
                        Instantiate(_goalPrefab, new Vector3(columnNum, rowNum, 1), Quaternion.identity);
                        break;

                    /* If the char is anything else, don't add a node for it.
                        This is done to avoid creating extra blank spaces due to 
                        CR chars or somesuch. */
                    default:
                        continue;
                }

                /* Add a node to the map for each char. */
                MapNode eachNode = new MapNode(nodeCoordinates, isNodeClear, isNodeStart, isNodeGoal);
                _map[rowNum].Add(eachNode);

                /* Keep a reference to this node if it's the start or goal. */
                if (eachNode._isStart)
                {
                    _startNode = eachNode;
                }
                else if (eachNode._isGoal)
                {
                    _goalNode = eachNode;
                }

                columnNum++;
            }

            rowNum++;
        }
    }

    /**
        Initialize the AI agent with world information.
     */
    private void InitializeAgent()
    {
        _agent = Instantiate(_agentPrefab, 
            new Vector3(_startNode._coordinates.x, _startNode._coordinates.y, _startNode._coordinates.z), 
            Quaternion.identity).GetComponent<Agent>();
        
        _agent._map = _map;
        _agent._startNode = _startNode;
        _agent._goalNode = _goalNode;
        _agent._messageBox = _messageBox;
    }

    /**
        Prints the flipped map to the debug log as text.
        Used for debugging.
     */
    private void PrintFlippedMap()
    {
        string flippedMap = "";
        
        foreach (List<MapNode> row in _map)
        {
            foreach (MapNode node in row)
            {
                if (node._isClear)
                {
                    flippedMap += " ";
                }
                else if (node._isStart)
                {
                    flippedMap += "S";
                }
                else if (node._isGoal)
                {
                    flippedMap += "G";
                }
                else
                {
                    flippedMap += "X";
                }
            }

            flippedMap += "\r";
        }
        Debug.Log(flippedMap);
    }
}
