﻿/*========================================================================================
    MapNode                                                                          *//**
	
    A node in a map that will use pathfinding.
	
    Copyright 2017 Erick Fernandez de Arteaga. All rights reserved.
        https://www.linkedin.com/in/erick-fda
        https://bitbucket.org/erick-fda
        
	@author Erick Fernandez de Arteaga
    @version 0.0.0
	@file
	
*//*====================================================================================*/

/*========================================================================================
	Dependencies
========================================================================================*/
using UnityEngine;

/*========================================================================================
	Structs
========================================================================================*/
/**
    A struct to reperesent a set of 3D int coordinates.
 */
public struct IntVector3
{
    public int x;
    public int y;
    public int z;

    public IntVector3(int _x, int _y, int _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }

    public static bool operator==(IntVector3 a, IntVector3 b)
    {
        return (a.x == b.x && 
            a.y == b.y && 
            a.z == b.z);
    }

    public static bool operator!=(IntVector3 a, IntVector3 b)
    {
        return !(a == b);
    }
}

/*========================================================================================
	MapNode
========================================================================================*/
/**
    A node in a map that will use pathfinding.
*/
public class MapNode
{
    /*------------------------------------------------------------------------------------
		Instance Fields
	------------------------------------------------------------------------------------*/
	
    
    /*------------------------------------------------------------------------------------
		Instance Properties
	------------------------------------------------------------------------------------*/
    public IntVector3 _coordinates { get; set; }
	public bool _isClear { get; set; }
    public bool _isStart { get; set; }
    public bool _isGoal { get; set; }

    public float _g { get; set; }
    public int _h { get; set; }
    
    /* Readonly _f is the sum of _g and _h. */
    public float _f
    { 
        get
        {
            return _g + _h;
        }
    }

    public MapNode _parent { get; set; }

    public Vector3 _position
    {
        get
        {
            return new Vector3(_coordinates.x, _coordinates.y, _coordinates.z);
        }
    }
    
	/*------------------------------------------------------------------------------------
		Constructors & Destructors
	------------------------------------------------------------------------------------*/
    public MapNode(IntVector3 coordinates, bool isClear, bool isStart, bool isGoal)
    {
        _coordinates = coordinates;
        _isClear = isClear;
        _isStart = isStart;
        _isGoal = isGoal;
        _g = 0;
        _h = 0;
        _parent = null;
    }
    
    /*------------------------------------------------------------------------------------
		Instance Methods
	------------------------------------------------------------------------------------*/
	/**
        Calculates the heuristic value for this node given the coordinates of the goal 
        node.
     */
    public void CalculateH(IntVector3 goalCoordinates)
    {
        _h = ManhattanDistance(goalCoordinates);
    }

    /**
        Returns the Manhattan distance between this node's coordinates and the given 
        coordinates.
     */
    public int ManhattanDistance(IntVector3 other)
    {
        int distance = 0;
        distance += Mathf.Abs(_coordinates.x - other.x);
        distance += Mathf.Abs(_coordinates.y - other.y);
        return distance;
    }

    /**
        Displays a path marker for this node and all of its ancestors.
     */
    public void DisplayPathMarker(GameObject _markerPrefab)
    {
        Vector3 location = new Vector3(_coordinates.x, _coordinates.y, _coordinates.z);
        GameObject.Instantiate(_markerPrefab, location, Quaternion.identity);

        if (_parent != null)
        {
            _parent.DisplayPathMarker(_markerPrefab);
        }
    }

    /**
        Returns whether this node is diagonally adjacent to the given node.
     */
     public bool IsDiagonallyAdjacentTo(MapNode other)
     {
         bool isOneStepHorizontal = false;
         bool isOneStepVertical = false;
         
         if (Mathf.Abs(this._coordinates.x - other._coordinates.x) == 1)
         {
             isOneStepHorizontal = true;
         }

        if (Mathf.Abs(this._coordinates.y - other._coordinates.y) == 1)
        {
            isOneStepVertical = true;
        }

        return (isOneStepHorizontal && isOneStepVertical);
     }
}
