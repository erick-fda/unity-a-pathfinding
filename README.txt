==========================================================================================
    COMP 8901 Assignment 02
    
    Copyright 2017 Erick Fernandez de Arteaga. All rights reserved.
        https://www.linkedin.com/in/erick-fda
        https://bitbucket.org/erick-fda

    Version 0.1.0
    
==========================================================================================

------------------------------------------------------------------------------------------
    TABLE OF CONTENTS
    
    ## Overview
    
------------------------------------------------------------------------------------------

==========================================================================================
    ## Overview
==========================================================================================
    I have written this assignment as a Unity project in C#.
    
    The pathfinding algorithm uses A* pathfinding. The algorithm allows for diagonal 
    movement, but will avoid cutting too closely to corners of obstacles. The algorithm 
    accounts for the difference in cost between lateral and diagonal movement: a lateral 
    step adds 1 to the target node's G value, while a diagonal step adds sqrt(2). The 
    algorithm uses the Manhattan distance between nodes as the heuristic for the distance 
    to the goal.
    
    The pathfinding algorithm can be found in 
    COMP8901_Asg02_A00925871/Assets/_Scripts/Agent.cs in the Pathfinding() method.
    
    The movement algorithm can be found in 
    COMP8901_Asg02_A00925871/Assets/_Scripts/Agent.cs in the Update() method.
    
    The program takes one text file in the previously agreed format as an input.
    To supply this file as input, place the file (named "map.txt") in the 
    COMP8901_Asg02_A00925871/Build/COMP8901_Asg02_A00925871_Data/Maps/ directory and run 
    the executable located at 
    COMP8901_Asg02_A00925871/Build/COMP8901_Asg02_A00925871.exe.
    
    While the program is running, pressing the spacebar will re-read map.txt and re-run 
    the pathfinding and movement algorithms. The map.txt file can be changed without 
    closing the program, so various maps can be easily tested. Pressing ESC will close 
    the program.
    